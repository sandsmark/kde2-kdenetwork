kde2_stdicon()

qt2_wrap_cpp(kdict_MOC
    SOURCES
        kdictapplet.h
)

kde2_module(libkdictapplet
    SOURCES
        kdictapplet.cpp
        ${kdict_MOC}
    LIBS
        kde2::kdeui
        kde2::ksycoca
    )

target_include_directories(module_libkdictapplet PRIVATE)

install(FILES kdictapplet.desktop DESTINATION ${KDE2_DATADIR}/kicker/applets)
