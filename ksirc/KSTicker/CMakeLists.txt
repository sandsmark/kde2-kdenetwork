qt2_wrap_cpp(ksticker_MOC
    SOURCES
        main.h
        ksticker.h
        speeddialog.h
        speeddialogData.h
    )
add_executable(ksticker
        main.cpp
        kspainter.cpp
        ksticker.cpp
        speeddialog.cpp
        speeddialogData.cpp
        ${ksticker_MOC}
    )

target_include_directories(ksticker PRIVATE ${CMAKE_CURRENT_BINARY_DIR})
target_link_libraries(ksticker kde2::kdeui)
install(TARGETS ksticker)
