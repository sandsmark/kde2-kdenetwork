install(
    FILES
        green-bullet.png kmfldin.png kmfldout.png kmfolder.png kminsorgmsg.png
        kmmsgdel.png kmmsgnew.png kmmsgunseen.png kmmsgold.png kmmsgreplied.png
        kmtrash.png red-bullet.png kmmsgforwarded.png kmmsgqueued.png
        kmmsgsent.png kmmsgflag.png kmfldsent.png closed.png green-bullet.png
        stopwatch.xbm stopwatchMask.xbm kdelogo.xpm feather_white.png
        pub_key_red.png pgp-keys.png kmfolderfull.png abup.png abdown.png 


    DESTINATION ${KDE2_DATADIR}/kmail/pics
    )

